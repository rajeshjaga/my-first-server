FROM node:12.16.3

WORKDIR "/code"

ENV PORT 80

COPY  ./code/package.json package.json

RUN npm i

COPY . /code

CMD ["node", "src/server.js"]